const mysql = require('mysql2')

const openConnection = () =>{
   const connection = mysql.createConnection({

        host: 'demoemp',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'empdb'
    })
    
    connection.connect();
    return connection
}

module.exports = {
    openConnection
}