const { response } = require('express')
const express = require('express')
const db = require('./db')

const app = express()

app.use(express.json())


app.get('/', (request, response)=>{
    const connection = db.openConnection()
    const sql = `SELECT * from emp`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

app.post('/', (request, response)=>{
    const connection = db.openConnection()
    const {empid, name, salary, age} = request.body
    const sql = `INSERT into emp VALUES (${empid}, '${name}', ${salary}, ${age})`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})


app.listen(4000, ()=>{
    console.log("started on port 4000")
})