CREATE TABLE emp (
  empid INTEGER PRIMARY KEY auto_increment,
  name VARCHAR(100),
  salary INTEGER,
  age INTEGER  
);

INSERT into emp VALUES ('1', 'Vishal',25000, 26);

INSERT into emp VALUES ('2', 'Sam',25000, 26);

INSERT into emp VALUES ('3', 'Virat',25000, 26);